;; # Exercice 4 : Collections

(ns theme01-clojure.ex04-collections
  (:use midje.sweet))


;; ## Question 1 : les vecteurs

(facts "pour l'accès aux informations des vecteurs"
  (and 
    
    (= (type [42 12 8]) :_______________________)
    
    (= [1 2 (+ 3 4)]  :______________________________)

    (= (count [42 12 8]) :______)
    
    (= (get [42 12 8] 0) :________)
    
    (= (get [42 12 8] 2) :________)
    
    (= (get [42 12 8] 3) :___________)
    
    (= (get [:a :b nil] 2) :_________)
    
    (= (get [42 12 8] 3 :bad) :______)
    
    (= (let [vs [42 12 8]]
         [(vs 0) (vs 2)]) :_________________)
    
	(= (try ([42 12 8] 3)
         (catch  java.lang.IndexOutOfBoundsException e :boum))
       
       :_________________)
    
    
    (= (let [[a b] [1 2]]
         b) 
       
       :_______)
    
    
    (= (let [[x y z] [2 3 4]]
         (+ x (- y z)))
       
       :_________))

  => true)

(facts "pour les mises-à-jour des vecteurs"
       (and 
         
         (= (conj [3 2 1] 4) :____________________)
         
         (= (conj [3 2 1] 4 5 6) :________________)
        
        
         (= (let [v [:a :b :c :d]
                  w (conj v :e)]
                [v w]) :_________________________)
       
       
         (= (assoc [3 2 1] 1 42) :__________________)

         (= (let [v [:a :b :c :d]
                  w (assoc v 2 :z)]
              [v w]) :_________________________))

       =>	true)

;; ## Question 2 : les listes

(facts 
  (and
    
    (= (type '(1 2 3)) :_____________________________________)
    
    (= '(1 2 (+ 3 4))  :______________________________)
    
    (= (list 1 2 (+ 3 4)) :_____________________)
  
    (= (conj '(1 2 3) 4) :________________________)
 
    (= (conj '(1 2 3) 4 5 6) :________________________)
    
    (= (count '(:a :b :c :d)) :__________)

    (= (first '(:a :b :c :d))  :____________)
    
    (= (rest '(:a :b :c :d)) :________________________)
    
    (= (rest (rest '(:a :b :c :d))) :________________________)

    (= (second '(:a  :b :c :d))  :__________)

    (= (first (rest '(:a  :b :c :d)))  :_________)
    
    (= (peek '(:a :b :c :d)) :_________)
    
    (= (pop '(:a :b :c :d)) :_____________________)
    
    (= (let [stk1 '(1 2 3 4)
             stk2 (pop (pop stk1))]
         [stk1 stk2])
       
       :________________________________________))
    
  => true)

;; ## Question 3 : les maps (tables associatives)

(facts "pour l'accès aux informations des vecteurs"
  (and 
    
    (= (type {:a 42 :b 12 :c 8}) :___________________________)
    
    (= (count {:a 42 :b 12 :c 8}) :_____)
    
    (= (get {:a 42 :b 12 :c 8} :b) :__________ )

    (= (get {:a 42 :b 12 :c 8} :e) :__________ )

    (= (get {:a 42 :b 12 :c 8} :e :not-found) :________________ )

    
    (= (let [m {:a 42 :b (+ (* 3 10) 12) :c 8}]
         
         (m :b)) :_________)

    
    (= (let [m {:a 42 :b 12 :c 8}]
         
         (m :e)) :_________)

    
    (= (let [m {"un" 1 "two" 2 "three" 3}]
         
         (m "two")) :_________)

    
    (= (let [m {:a 42 :b (+ (* 3 10) 12) :c 8}]
         
         (:b m)) :_________)

    
    (= (let [m {:a 42 :b (+ (* 3 10) 12) :c 8}]
         
         (:e m)) :_________)
    
    
    (= (contains? {:a 42 :b 12 :c 9} :b) :____________)
    
    (= (contains? {:a 42 :b 12 :c 9} :e) :____________)
    
    
    (= (= {:a 42 :b 12 :c 3} 
          {:c (+ 2 1) :a (* 21 2) :b (quot 144 12)}) :_____________))
    

  => true)

(facts "pour les mises-à-jour des maps"
       (and 
         
         (= (assoc {:a 42 :b 12 :c 8} :e 19)
            
            :______________________________)
         
         (= (assoc {:a 42 :b 12 :c 8} :b 42)
            
            :______________________________)
         
         (= (dissoc {:a 42 :b 12 :c 8} :b)
            
            :______________________________)

         (= (dissoc {:a 42 :b 12 :c 8} :e)
                     
            :______________________________))

       => true)

;; ## Question 4 : Les ensembles

(facts "pour les opérations de base des ensembles."
  (and
    
    (= (type #{13 24 3}) :_______________________________)
    
    (= (count #{13 24 3}) :_____________)
    
	(= (conj #{13 24 3} 12) :________________)
    
    (= (conj #{13 24 3} 24) :________________)
    
    (= (contains? #{13 24 3} 24) :_________)
    
    (= (contains? #{13 24 3} 12) :_________)

    (= (= (conj #{13 24 3} 12) #{3 12 13 24}) :__________))
    
 	 => true)

(facts "pour les opérations ensemblistes."
  (and

    (= (clojure.set/subset? 
         #{:a :b :c} 
         #{:z :d :b :e :c :g :a}) 
       
       :_________)
    
    (= (clojure.set/subset? 
         #{:z :d :b :e :c :g :a}
         #{:a :b :c}) 
       
       :_________)


    (= (clojure.set/union
         #{:a :b :c} 
         #{:z :d :b :e :c :g :a})
       
       :________________________________)
    
    (= (clojure.set/intersection
         #{:a :b :c} 
         #{:z :d :b :e :c :g :a})
       
       :________________________________)
    

    (= (clojure.set/difference
         #{:z :d :b :e :c :g :a}
         #{:a :b :c})
       
       :________________________________))
       
       
       => true)


